 <?php
$db_host = "127.0.0.1";
$db_user = "";
$db = "";
$db_password = "";
try {
	$db = new PDO("mysql:host=".$db_host.";charset=utf8mb4;dbname=".$db, $db_user, $db_password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
} catch(PDOException $e) {
    echo '<p class="bg-danger">'.$e->getMessage().'</p>';
    exit;
}
$succ = false;
if(isset($_POST['dl'])&&isset($_POST['up'])&&isset($_POST['ping'])){
	if(($_POST['dl'] >= 0&&$_POST['dl'] <= 1000)&&($_POST['up'] >=0&&$_POST['up'] <= 1000)&&($_POST['ping'] > 0&&$_POST['ping'] < 10000)){
		sleep(3);
		$succ = true;
		
	}
	
	
}


if($succ){
	function distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo) {
    $rad = M_PI / 180;
    $theta = $longitudeFrom - $longitudeTo;
    $dist = sin($latitudeFrom * $rad) * sin($latitudeTo * $rad) + cos($latitudeFrom * $rad) * cos($latitudeTo * $rad) * cos($theta * $rad);
    return acos($dist) / $rad * 60 * 1.853;
}
	$ip = "";
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['X-Real-IP'])) {
    $ip = $_SERVER['X-Real-IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    $ip = preg_replace("/,.*/", "", $ip); # hosts are comma-separated, client is first
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

    $isp = "";
	$rawIspInfo=null;
    try {
        $json = file_get_contents("https://ipinfo.io/" . $ip . "/json");
        $details = json_decode($json, true);
		$rawIspInfo=$details; 
        if (array_key_exists("org", $details))
            $isp .= $details["org"];
        else
            $isp .= "Unknown ISP";
        if (array_key_exists("country", $details))
            $isp .= ", " . $details["country"];
        $clientLoc = NULL;
        $serverLoc = NULL;
        if (array_key_exists("loc", $details))
            $clientLoc = $details["loc"];
            if ($clientLoc) {
                $json = file_get_contents("https://ipinfo.io/json");
                $details = json_decode($json, true);
                if (array_key_exists("loc", $details))
                    $serverLoc = $details["loc"];
                if ($serverLoc) {
                    try {
                        $clientLoc = explode(",", $clientLoc);
                        $serverLoc = explode(",", $serverLoc);
                        $dist = distance($clientLoc[0], $clientLoc[1], $serverLoc[0], $serverLoc[1]);
              
                            $dist = round($dist, -1);
                        
                    } catch (Exception $e) {}
                }
            }
    } catch (Exception $ex) {
        $isp = "Unknown ISP";
    }
	
	$db->exec("INSERT INTO `results` (`id`, `date`, `dl`, `up`, `ping`, `ip`, `isp`, `country`, `km`) VALUES (NULL, '".time()."', '".htmlspecialchars($_POST['dl'])."', '".htmlspecialchars($_POST['up'])."', '".htmlspecialchars($_POST['ping'])."', '".$ip."', '".$isp."', '00', '".$dist."');");
	
	echo $db->lastInsertId();
	
	
	
}
?> 